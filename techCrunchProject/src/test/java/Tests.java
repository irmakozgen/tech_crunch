import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tests extends Base {

    private static Assertions Assert;

    public static void main(String[] args) {
        String s = System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lenovo\\Desktop\\cd\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://techcrunch.com/");

        //Maximizing the window
        driver.manage().window().maximize();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,630)");

        WebElement authorName1 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[1]/header/div/div/span/span/a"));
        assertEquals(true, authorName1.isDisplayed());
        System.out.println("There is an author name in 1. content");

        WebElement Img1 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(1) > footer > figure > picture > img"));
        assertEquals(true, Img1.isDisplayed());
        System.out.println("There is an image in the 1. content");


        WebElement authorName2 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[2]/header/div/div/span/span/a"));
        assertEquals(true, authorName2.isDisplayed());
        System.out.println("There is an author name in 2. content");

        WebElement Img2 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(2) > footer > figure > picture > img"));
        assertEquals(true, Img2.isDisplayed());
        System.out.println("There is an image in the 2. content");


        WebElement authorName3 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[3]/header/div/div/span/span/a"));
        assertEquals(true, authorName1.isDisplayed());
        System.out.println("There is an author name in 3. content");

        WebElement Img3 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(3) > footer > figure > picture > img"));
        assertEquals(true, Img3.isDisplayed());
        System.out.println("There is an image in the 3. content");

        WebElement authorName4 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[4]/header/div/div/span/span/a"));
        assertEquals(true, authorName1.isDisplayed());
        System.out.println("There is an author name in 4. content");

        WebElement Img4 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(5) > footer > figure > picture > img"));
        assertEquals(true, Img4.isDisplayed());
        System.out.println("There is an image in the 4. content");

        WebElement authorName5 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[5]/header/div/div/span/span/a"));
        assertEquals(true, authorName5.isDisplayed());
        System.out.println("There is an author name in 5. content");

        WebElement Img5 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(6) > footer > figure > picture > img"));
        assertEquals(true, Img5.isDisplayed());
        System.out.println("There is an image in the 5. content");

        WebElement authorName6 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[6]/header/div/div/span/span/a"));
        assertEquals(true, authorName5.isDisplayed());
        System.out.println("There is an author name in 6. content");

        WebElement Img6 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(7) > footer > figure > picture > img"));
        assertEquals(true, Img6.isDisplayed());
        System.out.println("There is an image in the 6. content");

        WebElement authorName7 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[6]/header/div/div/span/span/a"));
        assertEquals(true, authorName7.isDisplayed());
        System.out.println("There is an author name in 7. content");

        WebElement Img7 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(7) > footer > figure > picture > img"));
        assertEquals(true, Img7.isDisplayed());
        System.out.println("There is an image in the 7. content");

        WebElement authorName8 = driver.findElement(By.xpath("//*[@id=\"tc-main-content\"]/div[3]/div/div/div/article[7]/header/div/div/span/span/a"));
        assertEquals(true, authorName8.isDisplayed());
        System.out.println("There is an author name in 8. content");

        WebElement Img8 = driver.findElement(By.cssSelector("#tc-main-content > div:nth-child(3) > div > div > div > article:nth-child(9) > footer > figure > picture > img"));
        assertEquals(true, Img8.isDisplayed());
        System.out.println("There is an image in the 8. content");


        WebElement elementToClick = driver.findElement(By.xpath("//*[text()='All things in moderation, including moderation']"));
        elementToClick.click();

        String actualTitle = driver.getTitle();
        String expectedTitle = "All things in moderation, including moderation | TechCrunch";
        assertEquals(expectedTitle,actualTitle);
        System.out.println("Page title equals to url title");

        WebElement linkName= driver.findElement(By.linkText("subscribe here."));

        if(linkName.isDisplayed())
        {
            System.out.println("Yes link is there");
        }
        else
        {
            System.out.println("No link is there");
        }











       }
    }

