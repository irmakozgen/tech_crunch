import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public class Base {

    @BeforeAll
    public static void setUp(){

        System.out.println("Setup method initiated.");

    }
    @AfterAll
    public void tearDown(){

        System.out.println("Test finished.");

    }
}
